package edu.kit.informatik.mobilealerts.communication.notification;

import android.app.Notification;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import edu.kit.informatik.mobilealerts.model.Filter;
import edu.kit.informatik.mobilealerts.model.FilterItem;
import edu.kit.informatik.mobilealerts.store.FilterStore;

public class NotificationService extends NotificationListenerService {
    private static final String TAG = "NotificationService";
    private Output output;

    @Override
    public void onCreate() {
        super.onCreate();
        this.output = new Output(getBaseContext());
    }

    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
        super.onNotificationPosted(sbn);
        String text = extractString(sbn, Notification.EXTRA_TEXT);
        String summary = extractString(sbn, Notification.EXTRA_SUMMARY_TEXT);
        String subText = extractString(sbn, Notification.EXTRA_SUB_TEXT);
        String bigText = extractString(sbn, Notification.EXTRA_BIG_TEXT);
        String title = extractString(sbn, Notification.EXTRA_TITLE);
        String displayName = extractString(sbn, Notification.EXTRA_SELF_DISPLAY_NAME);
        String conversationTitle = extractString(sbn, Notification.EXTRA_CONVERSATION_TITLE);
        String packageName = sbn.getPackageName();
        isMatch(text, title, conversationTitle, packageName);
        //infoLog(text, title, conversationTitle, displayName, packageName, summary, subText, bigText);
    }

    private String extractString(StatusBarNotification sbn, String key) {
        // using char sequence, because some apps (e.g. discord) put a SpannableString into the bundle
        // and SpannableString implements char sequence.
        CharSequence content = sbn.getNotification().extras.getCharSequence(key);
        if (content == null)
            return null;
        else
            return content.toString();
    }

    private void isMatch(@Nullable String text,
                         @Nullable String title,
                         @Nullable String conversationTitle,
                         @Nullable String packageName) {
        for (FilterItem filterItem : FilterStore.getFilters()) {
            Filter f = filterItem.getFilter();
            if (matchHelper(f.getActivationString(), text, true)
                    && (matchHelper(f.getCorespondent(), title, true)
                    || matchHelper(f.getCorespondent(), conversationTitle, false))
                    && matchHelper(f.getApp().packageName, packageName, false)) {
                Log.i(TAG, "onNotificationPosted: FILTER MATCH");
                this.output.action(filterItem.getAction(), text);
            }
        }
    }


    private boolean matchHelper(@NonNull String f, @Nullable String n, boolean contains) {
        if (f.isEmpty()) return true;
        if (n == null) return false;
        if (contains) return n.contains(f);
        return n.equals(f);
    }

    // IMPORTANT TODO remove this when not in testing.
    private void infoLog(String text, String title, String conversationTitle, String displayName, String packageName, String summary, String subText, String bigText) {
        Log.d(TAG, "Title: " + title);
        Log.d(TAG, "Conversation title: " + conversationTitle);
        Log.d(TAG, "DisplayName: " + displayName);
        Log.d(TAG, "Text: " + text);
        Log.d(TAG, "Package: " + packageName);
        Log.d(TAG, "summary: " + summary);
        Log.d(TAG, "subText: " + subText);
        Log.d(TAG, "bigText: " + bigText);
        Log.d(TAG, "----------------");
    }
}
