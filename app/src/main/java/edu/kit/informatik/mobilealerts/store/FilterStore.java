package edu.kit.informatik.mobilealerts.store;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Iterator;

import edu.kit.informatik.mobilealerts.FilterListAdapter;
import edu.kit.informatik.mobilealerts.model.FilterItem;

public class FilterStore {
    @NonNull
    private static ArrayList<FilterItem> sFilters = new ArrayList<>();
    @Nullable
    private static FilterListAdapter sToUpdate;
    private FilterStore() {
    }

    public static void addItem(FilterItem item) {
        sFilters.add(item);
        if (sToUpdate != null) {
            sToUpdate.notifyDataSetChanged();
        }
    }

    public static void removeItem(FilterItem item) {
        sFilters.remove(item);
        assert sToUpdate != null;
        sToUpdate.notifyDataSetChanged();
    }

    public static void setsToUpdate(@NonNull FilterListAdapter sToUpdate) {
        FilterStore.sToUpdate = sToUpdate;
    }

    public static void removeAll() {
        for (Iterator<FilterItem> iterator = sFilters.iterator(); iterator.hasNext(); ) {
            iterator.next();
            iterator.remove();
        }
        if (sToUpdate != null) {
            sToUpdate.notifyDataSetChanged();
        }
    }

    /**
     * Should only be used for initial setup, not for individual access.
     *
     * @return list of all filters
     */
    @NonNull
    public static ArrayList<FilterItem> getFilters() {
        return sFilters;
    }

    public static void setFilters(ArrayList<FilterItem> filters) {
        sFilters = filters;
    }

}
