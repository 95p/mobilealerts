package edu.kit.informatik.mobilealerts;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.ArrayList;

import javax.security.auth.login.LoginException;

import de.hdodenhof.circleimageview.CircleImageView;


public class AppSelectionAdapter extends ArrayAdapter<ApplicationInfo> {
    private final ArrayList<ApplicationInfo> mApps;
    private final Context context;

    public AppSelectionAdapter(@NonNull Context context, int resource, ArrayList<ApplicationInfo> mApps) {
        super(context, resource);
        this.context = context;
        this.mApps = mApps;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.app_list_item, parent, false);
        }
        TextView appName = convertView.findViewById(R.id.app_name);
        CircleImageView appIcon = convertView.findViewById(R.id.app_icon_apps);
        ApplicationInfo info = mApps.get(position);
        PackageManager packageManager = context.getPackageManager();
        appName.setText(packageManager.getApplicationLabel(info));
        Drawable res = info.loadIcon(context.getPackageManager());
        appIcon.setImageDrawable(res);
        return convertView;
    }

    @Override
    public int getCount() {
        return mApps.size();
    }

    @Override
    public ApplicationInfo getItem(int i) {
        return mApps.get(i);
    }
}
