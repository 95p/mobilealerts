package edu.kit.informatik.mobilealerts.communication.esense;

import android.util.Log;

import androidx.annotation.NonNull;

import edu.kit.informatik.mobilealerts.io.esense.esenselib.ESenseConnectionListener;
import edu.kit.informatik.mobilealerts.io.esense.esenselib.ESenseManager;
import edu.kit.informatik.mobilealerts.io.esense.esenselib.ESenseSensorListener;
import edu.kit.informatik.mobilealerts.view.IConnectionSignal;

public class ConnectionListener implements ESenseConnectionListener {
    private static final String TAG = "ConnectionListener";

    private final IConnectionSignal connectionSignal;
    private final ESenseSensorListener sensorListener;

    public ConnectionListener(@NonNull IConnectionSignal connectionSignal, @NonNull ESenseSensorListener sensorListener) {
        this.connectionSignal = connectionSignal;
        this.sensorListener = sensorListener;
    }

    @Override
    public void onDeviceFound(ESenseManager manager) {
        Log.i(TAG, "onDeviceFound: ");
    }

    @Override
    public void onDeviceNotFound(ESenseManager manager) {
        Log.i(TAG, "onDeviceNotFound: ");
    }

    @Override
    public void onConnected(ESenseManager manager) {
        Log.i(TAG, "onConnected: ");
        manager.registerSensorListener(sensorListener, 60);
        connectionSignal.setState(true);
    }

    @Override
    public void onDisconnected(ESenseManager manager) {
        Log.i(TAG, "onDisconnected: ");
        connectionSignal.setState(false);
    }
}
