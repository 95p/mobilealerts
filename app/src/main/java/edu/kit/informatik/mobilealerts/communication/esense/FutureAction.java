package edu.kit.informatik.mobilealerts.communication.esense;

public interface FutureAction {
    void onAction();
}
