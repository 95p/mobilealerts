package edu.kit.informatik.mobilealerts.communication.notification;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.speech.tts.TextToSpeech;
import android.util.Log;

import androidx.annotation.NonNull;

import java.util.Locale;
import java.util.Random;

import edu.kit.informatik.mobilealerts.communication.esense.FutureAction;
import edu.kit.informatik.mobilealerts.communication.esense.SensorListener;
import edu.kit.informatik.mobilealerts.model.NotificationAction;


public class Output {

    private final Context context;
    private final TextToSpeech tts;
    private Long ttsId;
    private static final String TAG = "Output";

    public Output(@NonNull Context context) {
        this.context = context;
        this.ttsId = new Random().nextLong();
        this.tts = new TextToSpeech(context, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                Log.i(TAG, "onInit: TTS initiated");
                tts.setLanguage(Locale.GERMAN);
            }
        });
    }

    public void action(NotificationAction action, String text) {
        if (action.isOverwriteVibration()) vibrate();
        if (action.isOverwriteRing()) playSound();
        if (action.isTts()) speakMessage(text);
    }

    private void vibrate() {
        Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        final int duration = 1000;
        if (vibrator == null) return;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            vibrator.vibrate(VibrationEffect.createOneShot(duration, VibrationEffect.DEFAULT_AMPLITUDE));
        else vibrator.vibrate(duration);
    }

    private void playSound() {
        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
        MediaPlayer mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
        try {
            mediaPlayer.setDataSource(context, notification);
            mediaPlayer.prepare();
        } catch (Exception ignored) {
        }
        mediaPlayer.start();
    }

    private void speakMessage(String text) {
        Log.i(TAG, "speakMessage: ");
        FutureAction futureAction = () -> {
            Log.i("Future Action", "speakMessage: EXECUTING");
            Bundle channelSettings = new Bundle();
            channelSettings.putString(TextToSpeech.Engine.KEY_PARAM_STREAM, String.valueOf(AudioManager.STREAM_SYSTEM));
            tts.speak(text, TextToSpeech.QUEUE_ADD, channelSettings, Long.toString(ttsId++));
        };
        SensorListener.addFutureAction(futureAction);
    }
}

