package edu.kit.informatik.mobilealerts.view;

public interface IConnectionSignal {
    void setState(boolean connected);
}
