package edu.kit.informatik.mobilealerts;

import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import edu.kit.informatik.mobilealerts.communication.esense.ConnectionListener;
import edu.kit.informatik.mobilealerts.communication.esense.SensorListener;
import edu.kit.informatik.mobilealerts.io.esense.esenselib.ESenseManager;
import edu.kit.informatik.mobilealerts.model.FilterItem;
import edu.kit.informatik.mobilealerts.model.UnavailableFilterException;
import edu.kit.informatik.mobilealerts.store.FilterStore;
import edu.kit.informatik.mobilealerts.view.ConnectButton;
import edu.kit.informatik.mobilealerts.view.IConnectionSignal;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.BLUETOOTH;
import static android.Manifest.permission.BLUETOOTH_ADMIN;
import static android.content.SharedPreferences.Editor;
import static android.provider.Settings.ACTION_NOTIFICATION_LISTENER_SETTINGS;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private static final String PREFS_KEY = "Filters";
    private static final String ENABLED_NOTIFICATION_LISTENERS = "enabled_notification_listeners";
    private SharedPreferences mPrefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new FilterConfiguration(this));
        loadFilters();

        if (!isNotificationServiceEnabled()) {
            buildNotificationServiceAlertDialog()
                    .show();
        }
        requestPermission();
    }

    private void requestPermission() {
        String[] permissions = new String[]
                {ACCESS_FINE_LOCATION, BLUETOOTH_ADMIN, BLUETOOTH, ACCESS_COARSE_LOCATION};
        if (!Arrays.stream(permissions)
                .allMatch(p -> ContextCompat.checkSelfPermission(getApplicationContext(), p)
                        == PackageManager.PERMISSION_GRANTED))
            ActivityCompat.requestPermissions(this, permissions, 643);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }
        if (id == R.id.connect) {
            String name = "eSense-0099";
            IConnectionSignal connectionSignal = new ConnectButton(item, this);
            ESenseManager manager = new ESenseManager(name,
                    MainActivity.this.getApplicationContext(),
                    new ConnectionListener(connectionSignal, new SensorListener()));
            manager.connect(30000);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveFilters();
    }

    private void loadFilters() {
        this.mPrefs = getPreferences(MODE_PRIVATE);
        if (mPrefs.contains(PREFS_KEY)) {
            Set<String> set = mPrefs.getStringSet(PREFS_KEY, null);
            if (set == null) return;
            FilterStore.removeAll();
            for (String representation : set) {
                try {
                    FilterStore.addItem(new FilterItem(representation, this));
                } catch (UnavailableFilterException e) {
                    // Don't add unavailable filters
                    Log.i(TAG, "loadFilters: unavailable filter" + representation);
                }
            }
        }
    }

    private void saveFilters() {
        ArrayList<FilterItem> list = FilterStore.getFilters();
        Set<String> toSave = new HashSet<>();
        list.forEach(s -> toSave.add(s.toString()));
        Editor editor = mPrefs.edit();
        editor.putStringSet(PREFS_KEY, toSave);
        editor.commit();
    }

    private boolean isNotificationServiceEnabled() {
        String pkgName = getPackageName();
        final String flat = Settings.Secure.getString(getContentResolver(),
                ENABLED_NOTIFICATION_LISTENERS);
        if (!TextUtils.isEmpty(flat)) {
            final String[] names = flat.split(":");
            for (String name : names) {
                final ComponentName cn = ComponentName.unflattenFromString(name);
                if (cn != null) {
                    if (TextUtils.equals(pkgName, cn.getPackageName())) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private AlertDialog buildNotificationServiceAlertDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Notification Listener");
        alertDialogBuilder.setMessage("In order to detect alarm notifications you need to enable notification listener for this app.");
        alertDialogBuilder.setPositiveButton("yes",
                (dialog, id) -> startActivity(new Intent(ACTION_NOTIFICATION_LISTENER_SETTINGS)));
        alertDialogBuilder.setNegativeButton("no",
                (dialog, id) -> {
                });
        return (alertDialogBuilder.create());
    }
}