package edu.kit.informatik.mobilealerts.model;

/**
 * A filter can't be used anymore. The cause for this may be that the app is not installed anymore.
 */
public class UnavailableFilterException extends Exception {
    public UnavailableFilterException() {
    }

    public UnavailableFilterException(String message) {
        super(message);
    }

    public UnavailableFilterException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnavailableFilterException(Throwable cause) {
        super(cause);
    }
}
