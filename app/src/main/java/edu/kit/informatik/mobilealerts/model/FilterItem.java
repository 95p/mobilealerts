package edu.kit.informatik.mobilealerts.model;

import android.content.Context;

import androidx.annotation.NonNull;

import java.io.Serializable;

public class FilterItem implements Serializable {
    private final int VERSION = 0;
    private final Filter filter;
    private final NotificationAction action;
    public static final String SEPARATOR = ";";
    private static final String OBJ_SEPARATOR = SEPARATOR + SEPARATOR;

    public FilterItem(Filter Filter, NotificationAction action) {
        this.filter = Filter;
        this.action = action;
    }

    @NonNull
    public Filter getFilter() {
        return filter;
    }

    @NonNull
    public NotificationAction getAction() {
        return action;
    }

    public FilterItem(@NonNull String representation, @NonNull Context context) throws UnavailableFilterException {
        String[] objects = representation.split(OBJ_SEPARATOR);
        Filter filter = new Filter(objects[0], context);
        NotificationAction action = new NotificationAction(objects[1]);
        this.filter = filter;
        this.action = action;
    }

    @Override
    @NonNull
    public String toString() {
        return filter.toString() + OBJ_SEPARATOR + action.toString();
    }
}
