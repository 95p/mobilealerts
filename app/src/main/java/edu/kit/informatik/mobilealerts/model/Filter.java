package edu.kit.informatik.mobilealerts.model;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import androidx.annotation.NonNull;

import java.util.List;

public class Filter {
    private final String title;
    private final String activationString;
    private final String corespondent;
    private final ApplicationInfo app;
    private final String SEPARATOR = FilterItem.SEPARATOR;

    public Filter(@NonNull String title,
                  @NonNull String activationString,
                  @NonNull String corespondent,
                  @NonNull ApplicationInfo app) {
        this.title = title;
        this.activationString = activationString;
        this.corespondent = corespondent;
        this.app = app;
    }

    @NonNull
    public String getTitle() {
        return title;
    }

    @NonNull
    public String getActivationString() {
        return activationString;
    }

    @NonNull
    public String getCorespondent() {
        return corespondent;
    }

    @NonNull
    public ApplicationInfo getApp() {
        return app;
    }

    public Filter(String representation, Context context) throws UnavailableFilterException {
        String[] values = representation.split(SEPARATOR);
        title = values[0];
        activationString = values[1];
        corespondent = values[2];
        PackageManager pm = context.getPackageManager();
        String appPackage = values[3];
        List<PackageInfo> installedPackages = pm.getInstalledPackages(PackageManager.GET_META_DATA);
        for (PackageInfo s : installedPackages) {
            if (s.packageName.equals(appPackage)) {
                this.app = s.applicationInfo;
                return;
            }
        }
        throw new UnavailableFilterException();
    }

    @SuppressLint("DefaultLocale")
    @NonNull
    @Override
    public String toString() {
        return String.format("%s%s%s%s%s%s%s",
                title, SEPARATOR, activationString, SEPARATOR, corespondent, SEPARATOR, app.packageName);
    }
}