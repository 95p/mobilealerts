package edu.kit.informatik.mobilealerts.io.esense.esenselib;

/**
 * Status of sensor sampling
 */

public enum ESenseSamplingStatus {
    STARTED,
    DEVICE_DISCONNECTED,
    ERROR
}
