package edu.kit.informatik.mobilealerts.store;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;

import java.util.Iterator;
import java.util.List;

public class AppProvider {
    // Depending on how expensive the operations are it may be good to implement some sort of caching
    // in the future
    public static List<ApplicationInfo> getApps(Context context) {
        final PackageManager packageManager = context.getPackageManager();
        @SuppressLint("QueryPermissionsNeeded") List<ApplicationInfo> installedApplications =
                packageManager.getInstalledApplications(PackageManager.GET_META_DATA);

        Iterator<ApplicationInfo> iterator = installedApplications.iterator();
        while (iterator.hasNext()) {
            ApplicationInfo appInfo = iterator.next();
            if ((appInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0) {
                iterator.remove();
            }
        }
        return installedApplications;
    }
}
