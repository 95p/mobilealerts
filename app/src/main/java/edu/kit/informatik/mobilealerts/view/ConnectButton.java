package edu.kit.informatik.mobilealerts.view;

import android.app.Activity;
import android.content.Context;
import android.view.MenuItem;

/**
 * Connection between the {@link edu.kit.informatik.mobilealerts.communication.esense.ConnectionListener}
 * and the "Connect"-Button in the UI
 */
public class ConnectButton implements IConnectionSignal {
    private final MenuItem menuItem;
    private final Activity activity;

    public ConnectButton(MenuItem menuItem, Activity activity) {
        this.menuItem = menuItem;
        this.activity = activity;
    }

    @Override
    public void setState(boolean connected) {
        activity.runOnUiThread(() -> {
            if (connected) {
                menuItem.setTitle("connected");
            } else {
                menuItem.setTitle("connect");
            }
        });
    }
}
