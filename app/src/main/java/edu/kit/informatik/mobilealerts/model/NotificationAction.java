package edu.kit.informatik.mobilealerts.model;

import androidx.annotation.NonNull;

import static edu.kit.informatik.mobilealerts.model.FilterItem.SEPARATOR;

public class NotificationAction {
    /**
     * Should phone (and headphone) vibrate even if deactivated in the system?
     */
    private final boolean overwriteVibration;
    /**
     * Should phone (and headphone) make a sound even if deactivated in the system?
     */
    private final boolean overwriteRing;
    /**
     * Should the message be read (text to speech)?
     */
    private final boolean tts;

    public NotificationAction(boolean overwriteVibration, boolean overwriteRing, boolean tts) {
        this.overwriteVibration = overwriteVibration;
        this.overwriteRing = overwriteRing;
        this.tts = tts;
    }

    public boolean isOverwriteVibration() {
        return overwriteVibration;
    }

    public boolean isOverwriteRing() {
        return overwriteRing;
    }

    public boolean isTts() {
        return tts;
    }

    /**
     * @param representation should contain exactly three ";"
     */
    public NotificationAction(@NonNull String representation) {
        String[] actionAttributes = representation.split(SEPARATOR);
        this.overwriteVibration = Boolean.parseBoolean(actionAttributes[0]);
        this.overwriteRing = Boolean.parseBoolean(actionAttributes[1]);
        this.tts = Boolean.parseBoolean(actionAttributes[2]);
    }

    @NonNull
    @Override
    public String toString() {
        return overwriteVibration + SEPARATOR + overwriteRing + SEPARATOR + tts;
    }
}
