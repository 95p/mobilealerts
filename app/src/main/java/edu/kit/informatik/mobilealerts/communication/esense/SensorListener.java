package edu.kit.informatik.mobilealerts.communication.esense;

import android.util.Log;

import androidx.annotation.NonNull;

import java.util.Arrays;
import java.util.Date;

import edu.kit.informatik.mobilealerts.BuildConfig;
import edu.kit.informatik.mobilealerts.io.esense.esenselib.ESenseEvent;
import edu.kit.informatik.mobilealerts.io.esense.esenselib.ESenseSensorListener;

public class SensorListener implements ESenseSensorListener {

    private static final String TAG = "SensorListener";

    private static FutureAction futureAction;
    private static Date arrival = new Date(0);

    /**
     * Save 2s of data
     */
    private static final int SIZE = 120;
    @NonNull
    private final short[][] data = new short[120][3];
    private int dataPointer = 0;

    @Override
    public void onSensorChanged(ESenseEvent evt) {
        addToLog(evt.getAccel());
        Log.i(TAG, "onSensorChanged: " + Arrays.toString(evt.getGyro()));
        if (isNodding()) {
            Log.i(TAG, "onSensorChanged: NODDING");
            if (arrival.after(new Date(new Date().getTime() - 6000))) {
                futureAction.onAction();
                arrival = new Date(0);
            }
        }
    }

    private void addToLog(short[] measurement) {
        if (BuildConfig.DEBUG && !(measurement.length == 3)) {
            throw new AssertionError("Assertion failed");
        }
        data[Math.floorMod(dataPointer++, SIZE)] = measurement;
    }

    // TODO better nodding detection that takes more data into account
    private boolean isNodding() {
        final short[] m1 = data[Math.floorMod(dataPointer - 1, SIZE)];
        final short[] m2 = data[Math.floorMod(dataPointer, SIZE)];
        return lookDown(m1)
                && lookDown(m2)
                && movement(m1, m2);
    }

    private boolean lookDown(short[] measurement) {
        return (-measurement[1] + measurement[2] < 1500);
    }

    private boolean movement(short[] first, short[] second) {
        return Math.abs(Math.abs(first[0]) - Math.abs(second[0])) < 100
                && Math.abs(Math.abs(first[1]) - Math.abs(second[1])) > 200
                && Math.abs(Math.abs(first[2]) - Math.abs(second[2])) > 200;
    }

    public static void addFutureAction(FutureAction futureAction) {
        arrival = new Date();
        SensorListener.futureAction = futureAction;
    }
}
