package edu.kit.informatik.mobilealerts;

import android.content.Context;
import android.content.pm.PackageManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import edu.kit.informatik.mobilealerts.model.Filter;
import edu.kit.informatik.mobilealerts.model.FilterItem;

/**
 * List of the filters. Each filter can be tabbed and edited.
 */
public class FilterListAdapter extends RecyclerView.Adapter<FilterListAdapter.ViewHolder> {

    private final ArrayList<FilterItem> mItems;
    private final Context mContext;

    public FilterListAdapter(ArrayList<FilterItem> mItems, Context context) {
        this.mItems = mItems;
        this.mContext = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.filter_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        FilterItem item = mItems.get(position);
        Filter Filter = item.getFilter();
        PackageManager packageManager = mContext.getPackageManager();
        holder.appIcon.setImageDrawable(Filter.getApp().loadIcon(packageManager));
        holder.title.setText(Filter.getTitle());
        holder.activation.setText(String.format(mContext.getString(R.string.match), Filter.getActivationString()));
        holder.conversation.setText(String.format(mContext.getString(R.string.conversation), Filter.getCorespondent()));

        holder.parentLayout.setOnClickListener(new FilterConfiguration(mContext, item));
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final CircleImageView appIcon;
        private final TextView title;
        private final TextView activation;
        private final TextView conversation;
        private final LinearLayout parentLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.appIcon = itemView.findViewById(R.id.app_icon);
            this.title = itemView.findViewById(R.id.notification_title);
            this.activation = itemView.findViewById(R.id.activation_string);
            this.conversation = itemView.findViewById(R.id.converstation);
            this.parentLayout = itemView.findViewById(R.id.filter_item);
        }
    }
}
