package edu.kit.informatik.mobilealerts;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.Build;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;

import java.util.ArrayList;
import java.util.List;

import edu.kit.informatik.mobilealerts.model.Filter;
import edu.kit.informatik.mobilealerts.model.FilterItem;
import edu.kit.informatik.mobilealerts.model.NotificationAction;
import edu.kit.informatik.mobilealerts.store.AppProvider;
import edu.kit.informatik.mobilealerts.store.FilterStore;

import static android.content.pm.ApplicationInfo.CATEGORY_IMAGE;
import static android.content.pm.ApplicationInfo.CATEGORY_PRODUCTIVITY;
import static android.content.pm.ApplicationInfo.CATEGORY_SOCIAL;

public class FilterConfiguration implements View.OnClickListener {

    private final Context context;
    private FilterItem item = null;

    public FilterConfiguration(Context context, FilterItem item) {
        this.context = context;
        this.item = item;
    }

    public FilterConfiguration(Context context) {
        this.context = context;
    }

    /**
     * Comparator to sort in the order that they are most relevant for the user
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    private static int sortApps(ApplicationInfo app1, ApplicationInfo app2) {
        if (app1.category == CATEGORY_SOCIAL && app2.category != CATEGORY_SOCIAL)
            return -1;
        if (app2.category == CATEGORY_SOCIAL && app1.category != CATEGORY_SOCIAL)
            return 1;
        if (app1.category == CATEGORY_PRODUCTIVITY && app2.category != CATEGORY_PRODUCTIVITY)
            return -1;
        if (app2.category == CATEGORY_PRODUCTIVITY && app1.category != CATEGORY_PRODUCTIVITY)
            return 1;
        if (app1.category == CATEGORY_IMAGE && app2.category != CATEGORY_IMAGE)
            return -1;
        if (app2.category == CATEGORY_IMAGE && app1.category != CATEGORY_IMAGE)
            return 1;
        return 0;
    }

    @Override
    public void onClick(View view) {
        AlertDialog.Builder addDialog = new AlertDialog.Builder(context);
        final View root = View.inflate(context, R.layout.configuration_dialog, null);
        addDialog.setView(root);
        addDialog.setTitle("Configure Filter");

        EditText title = root.findViewById(R.id.filter_title);
        EditText activation = root.findViewById(R.id.activation);
        EditText conversation = root.findViewById(R.id.conversation);
        CheckBox vibrate = root.findViewById(R.id.vibrate);
        CheckBox ring = root.findViewById(R.id.ring);
        CheckBox tts = root.findViewById(R.id.tts);

        List<ApplicationInfo> infos = AppProvider.getApps(context);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            infos.sort(FilterConfiguration::sortApps);
        }
        AppSelectionAdapter adapter = new AppSelectionAdapter(context, android.R.layout.simple_list_item_1, new ArrayList<>(infos));
        Spinner appSelect = root.findViewById(R.id.apps_spinner);
        appSelect.setAdapter(adapter);

        if (item != null) { // not null cases are for edit of filter
            Filter Filter = item.getFilter();
            NotificationAction action = item.getAction();
            appSelect.setSelection(infos.indexOf(Filter.getApp()));
            title.setText(item.getFilter().getTitle());
            activation.setText(Filter.getActivationString());
            conversation.setText(Filter.getCorespondent());
            vibrate.setChecked(action.isOverwriteVibration());
            ring.setChecked(action.isOverwriteRing());
            tts.setChecked(action.isTts());
        }

        addDialog.setPositiveButton(R.string.save, (dialog, which) -> {
            Filter Filter = new Filter(title.getText().toString(),
                    activation.getText().toString(),
                    conversation.getText().toString(),
                    (ApplicationInfo) appSelect.getSelectedItem()
            );
            NotificationAction action = new NotificationAction(vibrate.isChecked(),
                    ring.isChecked(), tts.isChecked());
            if (item != null) {
                FilterStore.removeItem(item);
            }
            FilterItem toSave = new FilterItem(Filter, action);
            FilterStore.addItem(toSave);
        });
        addDialog.setNeutralButton(R.string.discard, (dialog, which) -> { });
        if (item != null) {
            addDialog.setNegativeButton(R.string.remove, (dialog, which) -> FilterStore.removeItem(item));
        }

        addDialog.show();
    }
}
